#ifndef GSL_MAIN_H
#define GSL_MAIN_H
/**
 * \file gsl_main.h
 *
 * \brief
 *      Main entry point for Graph Service Layer (GSL)
 *
 *  Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 *  SPDX-License-Identifier: BSD-3-Clause-Clear
 */

 #endif //GSL_MAIN_H
