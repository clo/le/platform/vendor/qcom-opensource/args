#ifndef _APM_API_VERSION_H_
#define _APM_API_VERSION_H_
/**
 * \file apm_api_version.h
 * \brief 
 *  	 This file contains the version information for Audio Procesing Manager API
 * 
 * \copyright
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/apm/apm_api_version.h#4 $
*/
// clang-format on

#define APM_API_VERSION                           1

/** API branch version of the Audio Processing Manager

  @versiontable{0.3,4.7,
  0 & Mainline version @tblendline}
 */
#define APM_API_BRANCH_VERSION                    0

/** @} */ /* end_addtogroup aapm_api_version */


#endif /* _APM_API_VERSION_H_ */
