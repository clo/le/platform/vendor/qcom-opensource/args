#ifndef _ARDEFS_H
#define _ARDEFS_H
/**
 * \file ar_defs.h
 * \brief 
 * 
 * \copyright
 *  Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 *  SPDX-License-Identifier: BSD-3-Clause-Clear
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/ar_utils/ar_defs.h#2 $
*/
// clang-format on

#include "ar_osal_types.h"

  
#endif /* _ARDEFS_H */

