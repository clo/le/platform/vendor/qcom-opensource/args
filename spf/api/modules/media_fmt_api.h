#ifndef MEDIA_FMT_API_H
#define MEDIA_FMT_API_H
/**
 * \file media_fmt_api.h
 * \brief 
 *  	 This file contains media format IDs and definitions
 * 
 * \copyright
 *  Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 *  SPDX-License-Identifier: BSD-3-Clause-Clear
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/modules/media_fmt_api.h#34 $
*/
// clang-format on

#include "ar_defs.h"
#include "media_fmt_api_basic.h"
#include "media_fmt_api_ext.h"


#endif /* MEDIA_FMT_API_H */
