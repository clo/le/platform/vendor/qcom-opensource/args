/**
 * \file sp_dc_api.h
 * \brief 
 *      This file contains DTMF Detection Event api
 * 
 * \copyright
 *  Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 *  SPDX-License-Identifier: BSD-3-Clause-Clear
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/modules/sp_dc_api.h#4 $
*/
// clang-format on

#ifndef SP_API_H
#define SP_API_H

#include "ar_defs.h"

#define EVENT_ID_SP_DC_DETECTION 0x0800119C

/*==============================================================================
   Type definitions
==============================================================================*/

/** @h2xmlp_parameter   {"EVENT_ID_SP_DC_DETECTION",
                          EVENT_ID_SP_DC_DETECTION}
    @h2xmlp_description { Direct Current Detection event raised by the Speaker Protection Module.}
    @h2xmlp_toolPolicy  { NO_SUPPORT}*/

#endif
