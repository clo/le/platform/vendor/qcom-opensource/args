#ifndef _LPASS_CORE_API_H_
#define _LPASS_CORE_API_H_
/**
 * \file lpass_core_api.h
 * \brief 
 *  	 This file contains lpass core api
 * 
 * \copyright
 *  Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 *  SPDX-License-Identifier: BSD-3-Clause-Clear
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/modules/lpass_core_api.h#6 $
*/
// clang-format on

/**
   Param ID for lpass core
   This ID should only be used under PRM module instance
*/


#define PARAM_ID_RSC_LPASS_CORE 0x0800102B

#endif /* _LPASS_CORE_API_H_ */
